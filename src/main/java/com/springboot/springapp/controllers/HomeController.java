package com.springboot.springapp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by dixits on 17-02-2018.
 */
@RestController
public class HomeController {

   @GetMapping("/")
   public String defaultPage() {
      return "Spring boot app running successfully. This is the default page";
   }

   @GetMapping("/welcome")
   public String welcome() {
     return  "welcome to spring boot app";
   }



}
